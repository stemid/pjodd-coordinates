# Pjodd coordinates backend

Simple backend made for Lambda to serve the pjodd coordinates client, found in the parent directory.

# Install & run

	$ pipenv install -r requirements.txt
	$ pipenv run flask run
