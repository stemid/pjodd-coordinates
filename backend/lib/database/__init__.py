from .dynamodb import DynamoDBDatabase
from .sql import SQLDatabase

dbBackends = {
    'sqlite': SQLDatabase,
    'dynamo': DynamoDBDatabase,
}

class Database(object):
    def __init__(self, **kw):
        dbBackend = kw.get('backend')

        if dbBackend == 'sqlite':
            self.db = SQLDatabase(**kw)
        else:
            self.db = DynamoDBDatabase(**kw)

