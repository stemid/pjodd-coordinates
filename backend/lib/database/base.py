
class BaseDatabase(object):

    def __init__(self, **kw):
        self.app = kw.get('app')
        self.config = kw.get('config')

    def create_node(self, **kw):
        raise NotImplemented

    def request_node(self, **kw):
        raise NotImplemented

    def update_node(self, **kw):
        raise NotImplemented

    def delete_node(self, **kw):
        raise NotImplemented

    def commit(self):
        raise NotImplemented
