from os import getcwd
from datetime import datetime
from flask_sqlalchemy import SQLAlchemy

from lib import APP_NAME
from lib.database.base import BaseDatabase


db = SQLAlchemy()

class Node(db.Model):

    node_id = db.Column(db.String(32), primary_key=True)
    created = db.Column(db.DateTime, default=datetime.utcnow)
    node_data = db.Column(db.JSON(none_as_null=True))


class SQLDatabase(BaseDatabase):

    def __init__(self, **kw):
        app = kw.get('app')
        config = kw.get('config')
        cur_cwd = getcwd()+'/'

        db_uri = '{db_method}:///{db_path}{db_conn_str}'.format(
            db_method=config.get(APP_NAME, 'db_method'),
            db_conn_str=config.get(APP_NAME, 'db_conn_str'),
            db_path=cur_cwd
        )
        app.config['SQLALCHEMY_DATABASE_URI'] = db_uri
        app.config['SQLALCHEMY_ECHO'] = config.getboolean(
            APP_NAME, 'debug'
        )
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

        db.init_app(app)
        db.create_all(app=app)

    def commit(self, **kw):
        db.session.commit()

    def create_node(self, **kw):
        nodeId = kw.get('node_id')
        nodeData = kw.get('node_data')
        new_node = Node(
            node_id=nodeId,
            node_data=nodeData
        )
        db.session.add(new_node)
        self.commit()
        return new_node

    def request_node(self, **kw):
        nodeId = kw.get('node_id')
        if nodeId:
            node = Node.query.filter_by(
                node_id=nodeId
            ).first()
            return node
        else:
            return Node.query.all()

    def update_node(self, **kw):
        nodeId = kw.get('node_id')
        nodeData = kw.get('node_data')
        node = kw.get('node')

        if not node:
            node = Node.query.filter_by(
                node_id=nodeId
            ).first()

        if not node:
            raise StandardError('Node not found')

        node.node_data = nodeData
        return node

    def delete_node(self, **kw):
        nodeId = kw.get('node_id')
        
        node = self.request_node(node_id=nodeId)

        if not node:
            raise StandardError('Node not found')

        deleted = db.session.delete(node)
        return deleted
