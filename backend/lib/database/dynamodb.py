from datetime import datetime
from pynamodb.connection import TableConnection
from pynamodb.models import Model
from pynamodb.attributes import (
    UnicodeAttribute, JSONAttribute, UTCDateTimeAttribute
)
from pynamodb import exceptions

from lib import APP_NAME
from lib.database.base import BaseDatabase


class Node(Model):
    class Meta:
        table_name = 'Node'

    node_id = UnicodeAttribute(hash_key=True)
    created = UTCDateTimeAttribute(default=datetime.utcnow)
    node_data = JSONAttribute(null=True)


class DynamoDBDatabase(BaseDatabase):

    def __init__(self, **kw):
        super(DynamoDBDatabase, self).__init__(**kw)
        hostname = kw.get('hostname')
        if not Node.exists():
            Node.create_table(
                read_capacity_units=1,
                write_capacity_units=1,
                wait=True
            )
        self.conn = TableConnection(host=hostname)

    def commit(self, **kw):
        return True

    def create_node(self, **kw):
        nodeId = kw.get('node_id')
        nodeData = kw.get('node_data')
        new_node = Node(
            node_id=nodeId,
            node_data=nodeData
        )
        new_node.save()
        return new_node

    def request_node(self, **kw):
        nodeId = kw.get('node_id')
        if nodeId:
            try:
                node = Node.get(nodeId)
            except exceptions.DoesNotExist:
                return None
            return node
        else:
            nodes = []
            for node in Node.scan():
                nodes.append(node)
            return nodes

    def update_node(self, **kw):
        nodeId = kw.get('node_id')
        nodeData = kw.get('node_data')
        node = kw.get('node')

        if not node:
            node = Node.get(nodeId)

        if not node:
            raise StandardError('Node not found')

        node.node_data = nodeData
        node.save()
        node.refresh()
        return node

    def delete_node(self, **kw):
        nodeId = kw.get('node_id')

        node = self.request_node(node_id=nodeId)

        if not node:
            raise StandardError('Node not found')

        node.delete()
        return 1
