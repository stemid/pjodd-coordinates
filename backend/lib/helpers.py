from flask.json import jsonify

def json_abort(statusCode, message=''):
    response = jsonify({
        'status_code': statusCode,
        'message': message
    })
    response.status_code = statusCode
    return response


