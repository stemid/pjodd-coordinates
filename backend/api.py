import json
from json.decoder import JSONDecodeError
from configparser import RawConfigParser
from os import environ
from flask import Flask, request
from flask.json import jsonify
from sqlalchemy.exc import IntegrityError
from flask_cors import CORS
import requests
from pprint import pprint as pp

from lib import NODE_DESCRIPTIONS, APP_NAME
from lib.database import Database
from lib.helpers import json_abort

app = Flask(APP_NAME)
config = RawConfigParser()

config[APP_NAME] = {
    'db_method': 'sqlite',
    'db_conn_str': 'db.sqlite',
    'debug': True
}

config_filename = '{env}.cfg'.format(
    env=environ.get('FLASK_ENV', 'development')
)
config.readfp(
    open(config_filename)
)

db = Database(
    backend=config.get(APP_NAME, 'db_method'),
    hostname=config.get(APP_NAME, 'db_hostname'),
    app=app,
    config=config
).db

cors_origins = config.get(APP_NAME, 'cors_origin')
if cors_origins != '*':
    cors_origins = cors_origins.split(',')

cors = CORS(app, resources={
    r'/*': {
        'origins': cors_origins
    }
})


@app.route('/api/sync/nodes', methods=['GET'])
def sync_nodes():
    jsonUrl = 'http://gateway-01.mesh.pjodd.se/hopglass/nodes.json'
    try:
        data = requests.get(jsonUrl).json()
    except Exception as e:
        return json_abort(500, str(e))

    nodes = data.get('nodes')
    total_nodes = []
    failed = []
    created = []
    updated = []

    for node in nodes:
        nodeinfo = node.get('nodeinfo')
        nodeId = nodeinfo.get('node_id')
        nodeinfo['description'] = NODE_DESCRIPTIONS.get(
            nodeId,
            ''
        )

        location = nodeinfo.get('location')
        if location and not node.get('latitude'):
            (
                node['latitude'],
                node['longitude']
            ) = location.get('latitude'), location.get('longitude')

        total_nodes.append(nodeId)

        #old_node = Node.query.filter_by(
        #    node_id=nodeId
        #).first()
        old_node = db.request_node(node_id=nodeId)

        if old_node:
            # Update existing node
            #old_node.node_data = {**old_node.node_data, **node}
            db.update_node(
                node=old_node,
                node_data={**old_node.node_data, **node}
            )
            updated.append(nodeId)
        else:
            try:
                #new_node = Node(
                #    node_id=nodeId,
                #    node_data={**node}
                #)
                #db.session.add(new_node)
                new_node = db.create_node(node_id=nodeId, node_data=node)
            except IntegrityError:
                failed.append(node)
                continue
            except Exception as e:
                return json_abort(500, str(e))

            created.append(nodeId)

    db.commit()
    return jsonify({
        'failed_nodes': failed,
        'created_nodes': created,
        'updated_nodes': updated,
        'total_nodes': total_nodes
    })

@app.route('/api/node/<nodeId>', methods=['PUT'])
def create_node(nodeId):
    try:
        node = db.request_node(node_id=nodeId)
    except Exception as e:
        return json_abort(500, str(e))

    if node:
        return jsonify(node.node_data)

    try:
        nodeData = json.loads(request.data)
        if not nodeData:
            nodeData = {}
        node = db.create_node(
            node_id=nodeId,
            node_data=nodeData
        )
        db.commit()
    except JSONDecodeError:
        return json_abort(500, 'Malformed JSON input')
    except Exception as e:
        return json_abort(
            500,
            'Unknown error: {error}'.format(
                error=str(e)
            )
        )

    return jsonify(node.node_data)

@app.route('/api/node', methods=['GET'])
def request_nodes():
    nodes = db.request_node(node_id=None)
    nodes_list = []
    for node in nodes:
        nodes_list.append({
            'node_id': node.node_id,
            **node.node_data
        })
    return jsonify(nodes_list)

@app.route('/api/node/<nodeId>', methods=['GET'])
def request_node(nodeId):
    node = db.request_node(node_id=nodeId)
    if not node:
        return json_abort(404, 'Node not found')
    return jsonify(node.node_data)

@app.route('/api/node/<nodeId>', methods=['POST'])
def update_node(nodeId):
    node = db.request_node(node_id=nodeId)

    if not node:
        return json_abort(404, 'Node not found')

    try:
        nodeData = json.loads(request.data)
        db.update_node(
            node_id=nodeId,
            node_data={
                **node.node_data, **nodeData
            }
        )
        db.commit()
    except JSONDecodeError:
        return json_abort(500, 'Malformed JSON input')
    except Exception as e:
        return json_abort(
            500,
            'Unknown error: {error}'.format(
                error=str(e)
            )
        )

    return jsonify({'message': 'Node update successful'})

@app.route('/api/node/<nodeId>', methods=['DELETE'])
def delete_node(nodeId):
    try:
        deleted = db.delete_node(node_id=nodeId)
        db.commit()
    except StandardError:
        return json_abort(404, 'Node not found')

    return jsonify({
        'message': '{nodes} deleted'.format(
            nodes=deleted
        )
    })

@app.route('/', methods=['GET'])
def api_status():
    return jsonify({
        'status': 'OK'
    })
