export default {
  locale: 'sv',
  apiUrl: process.env.VUE_APP_API_URL,
  mapboxAccessToken: process.env.VUE_APP_MAPBOX_ACCESS_TOKEN
}
