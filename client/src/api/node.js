import config from '../config'
import axios from 'axios'

//axios.defaults.headers.common['Authorization'] = localStorage.getItem('user-session_token')

export const apiCreateNode = (nodeId, nodeData) => new Promise((resolve, reject) => {
  var apiUrl = `${config.apiUrl}/node/${nodeId}`

  var p = axios.put(apiUrl, nodeData).then((resp) => {
    resolve(resp)
  }).catch((error) => {
    reject(error)
  })
  return p
})

export const apiRequestNode = (nodeId) => new Promise((resolve, reject) => {
  var apiUrl = `${config.apiUrl}/node`

  if (nodeId)
    apiUrl = `${config.apiUrl}/node/${nodeId}`

  var p = axios.get(apiUrl)
  p.then((resp) => {
    resolve(resp)
  }).catch((error) => {
    reject(error)
  })
  return p
})

export const apiUpdateNode = (nodeId, nodeData) => new Promise((resolve, reject) => {
  var apiUrl = `${config.apiUrl}/node/${nodeId}`

  var p = axios.post(apiUrl, nodeData).then((resp) => {
    resolve(resp)
  }).catch((error) => {
    reject(error)
  })
  return p
})

export const apiDeleteNode = (nodeId) => new Promise((resolve, reject) => {
  var apiUrl = `${config.apiUrl}/node/${nodeId}`

  var p = axios.delete(apiUrl).then((resp) => {
    resolve(resp)
  }).catch((error) => {
    reject(error)
  })
  return p
})

export const apiSyncNodes = () => new Promise((resolve, reject) => {
  var apiUrl = `${config.apiUrl}/sync/nodes`

  var p = axios.get(apiUrl).then((resp) => {
    resolve(resp)
  }).catch((error) => {
    reject(error)
  })
  return p
})

