import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import config from '../config'

// Routed components
import Nodes from '../pages/Nodes.vue'
import Map from '../pages/Map.vue'

var app_title = 'Pjodd'
const router = new VueRouter({
  routes: [
    {
      path: '/',
      name: 'nodes',
      component: Nodes,
      meta: {
        title: (to) => {
          return `${app_title} - Nodes`
        }
      }
    },

    {
      path: '/map',
      component: Map,
      meta: {
        title: (to) => {
          return `${app_title} - Map`
        }
      }
    },

  ]
})

router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = to.meta.title(to);
  })
})

export default router
