import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import store from './store'
import router from './router'

import 'vuetify/dist/vuetify.min.css'
import './scss/style.scss'

import {
  globalToasts,
  globalFilters
} from './helpers'

Vue.config.productionTip = false
Vue.use(Vuetify)

// Global filters
globalFilters.forEach((f) => {
  Vue.filter(f.name, f.filter)
})

const vue_app = new Vue({
    el: '#app',
    template: '<App/>',
    render: h => h(App),
    components: { App },
    store,
    router,
})
vue_app.$mount('#app')
