import moment from 'moment'
import {DateTime, Duration} from 'luxon'
import config from '../config'

export const humanFileSize = (bytes, si=true) => {
    var thresh = si ? 1000 : 1024;
    if(Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }
    var units = si
        ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
        : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
    var u = -1;
    do {
        bytes /= thresh;
        ++u;
    } while(Math.abs(bytes) >= thresh && u < units.length - 1);
    return bytes.toFixed(1)+' '+units[u];
}

export const shortenKey = (value) => {
  if (!value) return ''
  let tmpArray = value.split('/')
  tmpArray.shift()
  return tmpArray.join('/')
}

export const globalFilters = [
  {
    name: 'shortenKey',
    filter: shortenKey
  },
  {
    name: 'alwaysShowStr',
    filter: (value) => {
      return !!value ? value : '[UNKNOWN]'
    }
  },
  {
    name: 'formatBytes',
    filter: (value, formatSi=true) => {
      return humanFileSize(value, formatSi)
    }
  },
  {
    name: 'formatDuration',
    filter: (seconds) => {
      let _seconds = Math.round(seconds)
      var duration = DateTime.fromMillis(_seconds*1000)
      duration = duration.set({seconds: _seconds})
      return `${duration.toFormat('d')}`
    }
  },
  {
    name: 'formatDate',
    filter: (value, formatMethod) => {
      moment.locale(config.locale)

      // Check if it's a Date object and convert to isoformat string
      if (typeof value.getMonth === 'function') {
        value = moment(value).format()
      }

      var formatted_date = ''
      var format_options = [
          {
              method: 'fromNow',
              date: moment(String(value)).fromNow()
          },
          {
            method: 'full',
            date: moment(String(value)).format()
          },
      ]

      if (!value) return formatted_date
      format_options.forEach(obj => {
          if (formatMethod == obj.method) {
              formatted_date = obj.date
          }
      })

      return formatted_date
    }
  },
]
