import {globalFilters} from './filters'

// Convert global filters to a mixin for use as methods
export const globalFiltersMixin = {
  methods: globalFilters.reduce((obj, item) => {
    obj[item.name] = item.filter
    return obj
  }, {})
}

// https://stackoverflow.com/a/8260383/1662806
export const youtubeIdFromUrl = (url) => {
  var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
  var match = url.match(regExp);
  return (match&&match[7].length==11)? match[7] : false;
}

export const getSimpleType = (mimeType) => {
  var ret_type = 'image'

  switch (mimeType) {
    case 'image/jpeg':
      ret_type = 'image'
      break
    case 'image/png':
      ret_type = 'image'
      break
    case 'video/mp4':
      ret_type = 'video'
      break
    case 'video/webm':
      ret_type = 'video'
      break
    case 'youtube':
      ret_type = 'youtube'
      break
    default:
      ret_type = 'unknown'
  }

  return ret_type
}

// Return a simple string like 'image' or 'video' for various content types
export const globalMethodsMixin = {
  methods: {
    youtubeIdFromUrl: youtubeIdFromUrl,
    getSimpleType: getSimpleType,
  },

}

export const simpleTypeMixin = globalMethodsMixin
