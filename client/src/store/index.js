import Vue from 'vue'
import Vuex from 'vuex'

import node from './modules/node'
import search from './modules/search'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    node,
    search,
  },
  //strict: debug,
  strict: false // this is necessary to avoid an error when editing forms
                // that contain vuex state data.
})
