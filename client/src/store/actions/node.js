export const NODE_CREATE = 'node/create'
export const NODE_CREATE_SUCCESS = 'node/create/success'
export const NODE_CREATE_ERROR = 'node/create/error'

export const NODE_REQUEST = 'node/request'
export const NODE_REQUEST_SUCCESS = 'node/request/success'
export const NODE_REQUEST_ERROR = 'node/request/error'

export const NODE_UPDATE = 'node/update'
export const NODE_UPDATE_SUCCESS = 'node/update/success'
export const NODE_UPDATE_ERROR = 'node/update/error'

export const NODE_DELETE = 'node/delete'
export const NODE_DELETE_SUCCESS = 'node/delete/success'
export const NODE_DELETE_ERROR = 'node/delete/error'

export const NODE_SYNC = 'node/sync'
export const NODE_SYNC_SUCCESS = 'node/sync/success'
export const NODE_SYNC_ERROR = 'node/sync/error'

export const NODE_SET_LATLNG = 'node/set/latlng'
