import Vue from 'vue'

import {
  SEARCH_SET
} from '../actions/search'

const state = {
  search: ''
}

const getters = {}

const actions = {}

const mutations = {
  [SEARCH_SET]: (state, search) => {
    Vue.set(state, 'search', search)
  },
}

export default {
  state,
  getters,
  actions,
  mutations
}
