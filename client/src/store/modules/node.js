import Vue from 'vue'

import { 
  NODE_CREATE,
  NODE_CREATE_SUCCESS,
  NODE_CREATE_ERROR,

  NODE_REQUEST,
  NODE_REQUEST_SUCCESS,
  NODE_REQUEST_ERROR,

  NODE_UPDATE,
  NODE_UPDATE_SUCCESS,
  NODE_UPDATE_ERROR,

  NODE_DELETE,
  NODE_DELETE_SUCCESS,
  NODE_DELETE_ERROR,

  NODE_SYNC,
  NODE_SYNC_SUCCESS,
  NODE_SYNC_ERROR,

  NODE_SET_LATLNG,
} from '../actions/node'

import {
  apiCreateNode,
  apiRequestNode,
  apiUpdateNode,
  apiDeleteNode,
  apiSyncNodes,
} from '../../api/node'

const state = { 
  nodes: [],

  createStatus: '',
  createError: '',

  requestStatus: '',
  requestError: '',

  updateStatus: '',
  updateError: '',

  deleteStatus: '',
  deleteError: '',

  syncStatus: '',
  syncError: '',
}

const getters = {
  getNodeById: (state) => (nodeId) => {
    for (let i=0;i<state.nodes.length;i++) {
      if (nodeId == state.nodes[i].node_id)
        return state.nodes[i]
    }
  },
}

const actions = {
  [NODE_CREATE]: ({commit}, {nodeId, nodeData}) => {
    commit(NODE_CREATE)
    var p = apiCreateNode(nodeId, nodeData)
    p.then((resp) => {
      commit(NODE_CREATE_SUCCESS, resp)
    }).catch((error) => {
      commit(NODE_CREATE_ERROR, error)
    })
    return p
  },

  [NODE_REQUEST]: ({commit}, nodeId) => {
    commit(NODE_REQUEST)
    var p = apiRequestNode(nodeId)
    p.then((resp) => {
      commit(NODE_REQUEST_SUCCESS, resp)
    }).catch((error) => {
      commit(NODE_REQUEST_ERROR, error)
    })
    return p
  },

  [NODE_UPDATE]: ({commit}, {nodeId, nodeData}) => {
    commit(NODE_UPDATE)
    var p = apiUpdateNode(nodeId, nodeData)
    p.then((resp) => {
      commit(NODE_UPDATE_SUCCESS, resp)
    }).catch((error) => {
      commit(NODE_UPDATE_ERROR, error)
    })
    return p
  },

  [NODE_DELETE]: ({commit}, nodeId) => {
    commit(NODE_DELETE)
    var p = apiDeleteNode(nodeId)
    p.then((resp) => {
      commit(NODE_DELETE_SUCCESS, resp)
    }).catch((error) => {
      commit(NODE_DELETE_ERROR, error)
    })
    return p
  },

  [NODE_SYNC]: ({commit}) => {
    commit(NODE_SYNC)
    var p = apiSyncNodes()
    p.then((resp) => {
      commit(NODE_SYNC_SUCCESS, resp)
    }).catch((error) => {
      commit(NODE_SYNC_ERROR, error)
    })
    return p
  },

}

const mutations = {
  [NODE_CREATE]: (state) => {
    state.createStatus = 'loading'
  },
  [NODE_CREATE_SUCCESS]: (state, resp) => {
    Vue.set(state, 'nodes', resp.data)
    state.createStatus = 'success'
  },
  [NODE_CREATE_ERROR]: (state, error) => {
    state.createError = error
  },

  [NODE_REQUEST]: (state) => {
    state.requestStatus = 'loading'
  },
  [NODE_REQUEST_SUCCESS]: (state, resp) => {
    Vue.set(state, 'nodes', resp.data)
    state.requestStatus = 'success'
  },
  [NODE_REQUEST_ERROR]: (state, error) => {
    state.requestError = error
  },

  [NODE_UPDATE]: (state) => {
    state.updateStatus = 'loading'
  },
  [NODE_UPDATE_SUCCESS]: (state, resp) => {
    state.updateStatus = 'success'
  },
  [NODE_UPDATE_ERROR]: (state, error) => {
    state.updateError = error
    state.updateStatus = 'error'
  },

  [NODE_DELETE]: (state) => {
    state.deleteStatus = 'loading'
  },
  [NODE_DELETE_SUCCESS]: (state, resp) => {
    state.deleteStatus = 'success'
  },
  [NODE_DELETE_ERROR]: (state, error) => {
    state.deleteError = error
  },

  [NODE_SYNC]: (state) => {
    state.syncStatus = 'loading'
  },
  [NODE_SYNC_SUCCESS]: (state, resp) => {
    Vue.set(state, 'syncData', resp.data)
    state.syncStatus = 'success'
  },
  [NODE_SYNC_ERROR]: (state, error) => {
    state.syncError = error
  },

  [NODE_SET_LATLNG]: (state, {nodeId, latlng}) => {
    Vue.set(state, 'nodes', state.nodes.map((c) => {
      if (nodeId == c.node_id) {
        c.latitude = latlng.lat
        c.longitude = latlng.lng
      }
      return c
    }))
  },

}

export default {
  state,
  getters,
  actions,
  mutations,
}
