# Pjodd coordinates

This comes in two parts, start with backend. Here's how to get a development environment up.

# Backend

	$ cd backend/
	$ pipenv install
	$ FLASK_APP=api.py pipenv run flask run -p 8091 --reload

# Frontend

Copy the file .env.development to .env.development.local and change ``API_URL`` to match the backend you just started, and add your MapBox access token.

	$ cd client/
	$ yarn install
	$ yarn run serve --mode development

# Usage

Use the Sync nodes feature first to sync nodes from graph.pjodd.se into your local DB. Then you can start adding info to your local DB. Further node syncs will not overwrite your coordinates.

Deleting a node will lose your coordinates even if you re-sync it.
